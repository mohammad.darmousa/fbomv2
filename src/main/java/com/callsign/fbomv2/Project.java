package com.callsign.fbomv2;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Project {
    int projectId;
    String name;

    String branch = "develop";
}