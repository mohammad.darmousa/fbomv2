package com.callsign.fbomv2;

import com.squareup.tools.maven.resolution.ArtifactResolver;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.json.JSONObject;

import java.io.StringReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

//@SpringBootApplication
public class Fbomv2Application {

    public static void main(String[] args) throws Exception{
        //SpringApplication.run(Fbomv2Application.class, args);
        //TODO: remove me
        String token = "CHANGEME";




        var projects = getAllProjects();

        for (Project project : projects){

            String fileUrl = "https://gitlab-enterprise.a2org.com/api/v4/projects/"+project.projectId+"/repository/files/pom.xml?ref="+project.branch;

            // Create HTTP request with token
            HttpRequest request = HttpRequest.newBuilder()
                    .header("PRIVATE-TOKEN", token)
                    .uri(new URI(fileUrl))
                    .GET()
                    .build();

            HttpClient client = HttpClient.newHttpClient();

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            // Parse JSON
            JSONObject json = new JSONObject(response.body());

            // Extract attribute
            String fileContent = json.getString("content");


            byte[] decodedBytes = Base64.getDecoder().decode(fileContent);
            String decodedString = new String(decodedBytes);
            System.out.println(decodedString);

            MavenXpp3Reader reader = new MavenXpp3Reader();
            Model model = reader.read(new StringReader(decodedString));
            var resolver = new ArtifactResolver();

            printDependencyTree(model, resolver);
        }




    }

    private static void printDependencyTree(Model model, ArtifactResolver resolver) throws XmlPullParserException {

        var preoperties =  model.getProperties();
        var parentGroupId = model.getParent().getGroupId();
        var parentVersion = model.getParent().getVersion();

        HashMap<String, String> depManVersion = new HashMap<>();

        model.getDependencyManagement().getDependencies()
                .stream()
                .forEach(dep-> {

                    var varsion ="";
                    if(dep.getVersion().contains("$")){
                        var str = dep.getVersion()
                                .replace("$", "")
                                .replace("{", "")
                                .replace("}", "");
                        varsion = preoperties.getProperty(str);
                    }else{
                        varsion = dep.getVersion();
                    }
                    depManVersion.put(dep.getGroupId(), varsion);
                });





        for (Dependency dependency : model.getDependencies()) {
            if(dependency.getScope() !=null && dependency.getScope().equals("test")){
                continue;
            }

            if (!dependency.getArtifactId().contains("callsign")) {
                var id = dependency.getGroupId() + ":" + dependency.getArtifactId() +":" ;
                System.out.println(id);

                if(dependency.getGroupId().equals(parentGroupId) && dependency.getVersion() == null){
                    id +=  parentVersion;
                    dependency.setVersion(parentVersion);
                }
                else if (dependency.getVersion() != null && dependency.getVersion().contains("$")) {
                        var version = dependency.getVersion()
                                .replace("$", "")
                                .replace("{", "")
                                .replace("}", "");
                        id += preoperties.getProperty(version);

                } else if (dependency.getVersion() == null) {
                    if(depManVersion.containsKey(dependency.getGroupId())){
                        id += depManVersion.get(dependency.getGroupId());
                    }else {
                        System.out.println(id + "REPORT");
                        continue;
                    }

                } else {
                    id += dependency.getVersion();

                }


                System.out.println(id);
//                var artifact = resolver.artifactFor(id);
//                var resolvedArtifact = resolver.resolveArtifact(artifact);
//                var modelDep = resolvedArtifact.getModel();
//                System.out.println(depToString(id, modelDep, dependency));




            }
        }

    }

    public static String depToString(String id, Model model, Dependency dependency) {

        return id + "##" + dependency.getType() + "##" + model.getLicenses().get(0).getName() + "##" + model.getOrganization();
    }


    private static List<Project> getAllProjects() {
        return new ArrayList<>(){
            {add(Project.builder()
                    .projectId(369)
                    .name("Billing")
                    .branch("develop")
                    .build());}
        };

    }
}